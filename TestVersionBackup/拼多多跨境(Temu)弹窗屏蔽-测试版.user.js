﻿// ==UserScript==
// @name         拼多多跨境(Temu)弹窗屏蔽-测试版
// @version      0.6
// @description  用于屏蔽拼多多跨境卖家平台的弹窗.!!!普通用户请不要使用该版本，除非您能看懂出错原因用于反馈
// @antifeature  ads !!!普通用户请不要使用该版本，除非您能看懂出错原因用于反馈或者修改
// @author       linying
// @match        *://kuajing.pinduoduo.com/*
// @match        *://seller.kuajingmaihuo.com/*
// @match        *://kuajingboss.com/*
// @match        *://agentseller.temu.com/*
// @exclude      */login*
// @exclude      */settle/site-main*
// @exclude      */questionnaire?surveyId=*
// @exclude      */settle/seller-login?redirectUrl=*
// @exclude      */agentseller*.temu.com/main/authentication?redirectUrl=*
// @exclude      */agentseller*.temu.com/mmsos/online-shipping-result.html*
// @icon         https://gitlab.com/linying23333/green-service-center-temu-or-pinduoduokuajing-2024/raw/main/icon.svg
// @downloadURL  https://gitlab.com/linying23333/green-service-center-temu-or-pinduoduokuajing-2024/raw/main/%E6%8B%BC%E5%A4%9A%E5%A4%9A%E8%B7%A8%E5%A2%83(Temu)%E5%BC%B9%E7%AA%97%E5%B1%8F%E8%94%BD-%E6%B5%8B%E8%AF%95%E7%89%88.user.js
// @updateURL    https://gitlab.com/linying23333/green-service-center-temu-or-pinduoduokuajing-2024/raw/main/%E6%8B%BC%E5%A4%9A%E5%A4%9A%E8%B7%A8%E5%A2%83(Temu)%E5%BC%B9%E7%AA%97%E5%B1%8F%E8%94%BD-%E6%B5%8B%E8%AF%95%E7%89%88.user.js
// @supportURL   https://gitlab.com/linying23333/green-service-center-temu-or-pinduoduokuajing-2024
// @homepage     https://github.com/linying2333
// @run-at       document-idle
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// @grant        GM.setValue
// @grant        GM.getValue
// @grant        GM.deleteValue
// @grant        GM_registerMenuCommand
// ==/UserScript==
// @require      http://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js
// @note         (预留的jQuery库，以便未来引用)
// @icon         来自 https://www.iconfont.cn/ 如果侵犯您的权利请与我沟通
// @note         更新日志&常见问题解决：https://gitlab.com/linying23333/green-service-center-temu-or-pinduoduokuajing-2024/raw/main/Readme.md
// @note         本js用户脚本版权归linying所有,仅供研究学习,禁止以任何形式倒卖

'use strict';

/*
防止代码因其他原因被执行多次
这段代码出自 Via轻插件，作者谷花泰
*/
const key = encodeURIComponent('拼多多跨境(Temu)弹窗屏蔽');
if (window[key]) return;
window[key] = true;

// 初始化默认数据库
let value = [
    // 参数需要根据您的电脑加载速度而定,切勿无脑调低或者调高

    // 基础设置

    // 设置配置文件版本(为未来预留)
    {
        name: 'setting_Config_Version',
        value: '1'
    },

    // 启动后进行删除的等待时间:
    // 值设置为 0 不启用，默认推荐值为3800
    // 单位毫秒,1秒 = 1000毫秒
    {
        name: 'setting_Start_Wait_Time',
        value: '4200'
    },
    // 是否展示调价菜单
    // 默认true(开启),使用false(关闭)
    {
        name: 'setting_Show_Price_Menu',
        value: 'true'
    },

    // 快速模式设置

    // 快速删除模式的删除间隔时间:
    // 默认推荐值为100
    // 单位毫秒,1秒 = 1000毫秒
    {
        name: 'setting_Fast_Remove_Interval_Time',
        value: '1'
    },
    // 快速删除持续删除时间
    // 默认推荐值为8000
    // 单位毫秒,1秒 = 1000毫秒
    {
        name: 'setting_Fast_Remove_Duration',
        value: '1'
    }, // 'setting_Fast_Remove_Interval_Time'与'setting_Fast_Remove_Duration'同时设置为 1 不启用
    // 是否弹出快速删除结束提示框
    // 默认false(关闭),使用true(开启)
    {
        name: 'setting_Show_Fast_Remove_Stopped_Alert',
        value: 'false'
    },

    // 手动清除按钮设置

    // 设置是否添加手动清除按钮
    // 默认true(开启),使用false(关闭)
    {
        name: 'setting_Add_Manual_Clear_Button',
        value: 'true'
    },
    // 设置按钮加载位置默认值
    // 位置从浏览器屏幕左上角开始计算，X轴+1则向右移动，Y轴+1则向下移动
    // 参数需要带单位(受支持的单位 百分比"%",像素点"px")
    // 按钮的X轴(纵向)值
    {
        name: 'setting_Add_Manual_Clear_Button_X',
        value: '30%'
    },
    // 按钮的Y轴(纵向)值
    {
        name: 'setting_Add_Manual_Clear_Button_Y',
        value: '92%'
    },
    // 在多长时间内快速点击3次移动按钮打开设置
    // 默认推荐值为400
    // 单位毫秒,1秒 = 1000毫秒
    {
        name: 'setting_Quickly_Click_3Times_To_Open_Settings_Check_Time',
        value: '500'
    },

    // 调试模式日志输出

    // 默认false(关闭),使用true(开启)
    {
        name: 'setting_Print_DebugMode_Log',
        value: 'false'
    }];

value.forEach((Event) => {
    let setValue = Event.value; // 先获取原始值
    // 特别处理布尔类型的值
    if (Event.value === 'true' || Event.value === 'false') {
        setValue = Event.value === 'true';
    }

    // 如果没有对应的配置项，则为油猴存储添加配置项，确保值为期望的类型
    if (!GM_getValue(Event.name)) {GM_setValue(Event.name, setValue);};
});

// 初始化变量
var Button_X = GM_getValue('setting_Add_Manual_Clear_Button_X'),Button_Y = GM_getValue('setting_Add_Manual_Clear_Button_Y');

// 创建 GreaseMonkey 菜单
(function CreateMenu() {
    GM_registerMenuCommand('⚙️ 设置', () => {
        LoadSettingsPanel();
    });
    var Status = GM_getValue('setting_Print_DebugMode_Log');
    GM_registerMenuCommand('🛠️ 打印控制台调试日志状态切换 | 首次加载状态:' + Status, () => {
        Status = !Status
        // 将字符串转换为布尔值
        let boolValue = (Status === 'true') ? true : (Status === 'false') ? false : Status;
        GM_setValue('setting_Print_DebugMode_Log', boolValue);
        // 提示刷新网页使其生效
        alert('打印调试日志状态已经更新为' + boolValue + '\n请手动刷新网页使其油猴菜单文字刷新');
    });
})();

// 全局定义是否打印日志调用函数
function log(message) {
    if (GM_getValue('setting_Print_DebugMode_Log')) {
        console.log('来自 拼多多跨境(Temu)弹窗屏蔽 js用户脚本提示：\n' + message);
    }
}

// 提示调试模式是否开启
(function() {
    log(`您的 Debug 模式值为 ${GM_getValue('setting_Print_DebugMode_Log')} ,调试模式打印日志已经启用`)
})();

// 按钮处理部分
// 检查是否启用该部分
if (GM_getValue('setting_Add_Manual_Clear_Button')) {
    (function() {
        // 创建新的div
        var div = document.createElement('div');
        div.id = 'js_button_div'; // 设置id

        //禁止选择文字
        div.style.userSelect = 'none'; // 对于Webkit和Mozilla浏览器，IE浏览器应使用'unselectable'
        div.style.MozUserSelect = 'none'; // 对于早期的Firefox
        div.style.webkitUserSelect = 'none'; // 对于早期的Chrome和Safari
        div.style.msUserSelect = 'none'; // IE 10+

        div.style.cssText = `
          z-index: 2147483648 !important;
          position: fixed;
          top: ${GM_getValue('setting_Add_Manual_Clear_Button_Y')};
          left: ${GM_getValue('setting_Add_Manual_Clear_Button_X')};
        `; // 使用了变量

        // 添加div到boby外
        document.body.insertAdjacentElement("afterend", div);

        // 创建移动按钮元素
        var eventMoveButton = document.createElement('button');
        eventMoveButton.id = 'js_MoveButton';
        eventMoveButton.style.cssText = ` cursor: move; /* 切换用户鼠标为移动图标 */ `;
        eventMoveButton.textContent = '🔧';
        document.getElementById('js_button_div').appendChild(eventMoveButton); // 确保按钮添加到div后才操作

        // 创建一个新的按钮元素
        var CleanButton = document.createElement('button');
        CleanButton.id = 'js_CleanButton';
        CleanButton.textContent = '清除弹窗!';

        CleanButton.style.cssText = ` cursor: pointer; /* 鼠标悬停时显示手型 */ `;

        // 为按钮添加点击事件监听器
        CleanButton.onclick = function() {
            log('手动清除按钮被点击');
            removeElements();
            alert('已经执行清除');
        };

        document.getElementById('js_button_div').appendChild(CleanButton); // 确保清除按钮添加到div中

        // 初始化按钮功能
        var isDraggable = false; // 初始化拖动状态为禁用
        var eventDiv = document.getElementById('js_button_div');

        // 拖动逻辑
        document.getElementById('js_MoveButton').addEventListener('mousedown', function(mouseDownEvent) {
            if (!isDraggable) return; // 如果不可拖动，则不执行拖动逻辑
            function onMouseMove(mouseMoveEvent) {
                var newX = mouseMoveEvent.clientX;
                var newY = mouseMoveEvent.clientY;
                eventDiv.style.left = newX + 'px';
                eventDiv.style.top = newY + 'px';
            }

            document.addEventListener('mousemove', onMouseMove);
            document.addEventListener('mouseup', function() {
                document.removeEventListener('mousemove', onMouseMove);
                log("最终位置：(X: " + eventDiv.style.left + ", Y: " + eventDiv.style.top + ")");
                // 将目前的值加载到变量，使得当窗口不存在也能更新值
                Button_X = eventDiv.style.left ;Button_Y = eventDiv.style.top;
                // 检查是否可以直接注入变量实时加载当前值到窗口
                if (document.getElementById('js_Button_X')) {document.getElementById('js_Button_X').value = eventDiv.style.left;};
                if (document.getElementById('js_Button_Y')) {document.getElementById('js_Button_Y').value = eventDiv.style.top;};
            }, {once: true});

        });

        var clickCount = 0; // 用来跟踪点击次数的变量
        var clickTimer; // 用来存储定时器的变量

        // 清除点击计数的函数
        function clearClickCount() {
            clickCount = 0;
            clearTimeout(clickTimer);
        }

        // 添加连续点击三次的逻辑
        document.getElementById('js_MoveButton').addEventListener('click', function(event) {
            // 切换拖动状态
            isDraggable = !isDraggable;
            this.textContent = isDraggable ? '📝' : '🔧';

            // 增加点击计数
            clickCount++;

            // 清除之前的定时器，并设置新的定时器
            clearTimeout(clickTimer);
            clickTimer = setTimeout(function() {
                // 如果在指定时间内（例如 X 毫秒）发生了三次点击
                if (clickCount >= 3) {
                    document.getElementById('js_MoveButton').textContent = '⚙';
                    log('连续点击了三次！为您打开参数设置');
                    LoadSettingsPanel()
                    // 重置点击计数
                    clearClickCount();
                } else {
                    // 否则，只重置点击计数
                    clearClickCount();
                }
            }, GM_getValue('setting_Quickly_Click_3Times_To_Open_Settings_Check_Time')); // 定义连续点击的时间段为 X 毫秒
        });
    })();
}

// 插入参数设置面板
function LoadSettingsPanel() {

    if (document.getElementById('js_info')){
        return; // 发现已打开，退出函数
    }

    // 读取列表获取值
    function getDefaultValue(name) {
        for (const item of value) { // 修改这里，直接使用外部定义的value数组
            if (item.name === name) {
                return item.value;
            }
        }
        return "Not found"; // 如果没有找到对应name的值，返回一个默认信息
    }

    // 直接注入html
    insertModalDivs(`
        <h1 style="margin: 0; padding: 0;">设置参数</h1>
        <div style="margin-top: 5px; margin-bottom: 5px; color: #f00;">
        <p style="margin: 0; padding: 0;">注意前后不要有空格，填写错误会导致运行错误</p>
        <p style="margin: 0; padding: 0;">时间值默认单位 ms (毫秒),1 s(秒) = 1000 ms(毫秒)</p>
        <p style="margin: 0; padding: 0;">功能开关：使用 true (开启),使用 false (关闭)</p>
        </div>
        <div style="margin: 0; padding: 0;">
          <p style="margin: 0; padding: 0;">启动后进行删除的等待时间:</p>
          <input type="text"
                 class="js_setting_input"
                 style="background-color: #808080; margin: 3px 0; padding: 2px;"
                 placeholder="默认值：${getDefaultValue('setting_Start_Wait_Time')}"
                 value="${GM_getValue('setting_Start_Wait_Time')}">
        </div>
        <p></p>
        <div style="margin: 0; padding: 0;">
          <p style="margin: 0; padding: 0;">是否展示调价菜单:</p>
          <input type="text"
                 class="js_setting_input"
                 style="background-color: #808080; margin: 3px 0; padding: 2px;"
                 placeholder="默认值：${getDefaultValue('setting_Show_Price_Menu')}"
                 value="${GM_getValue('setting_Show_Price_Menu')}">
        </div>
        <p></p>
        <div style="margin: 0; padding: 0;">
          <p style="margin: 0; padding: 0;">快速删除模式的删除间隔时间:</p>
          <input type="text"
                 class="js_setting_input"
                 style="background-color: #808080; margin: 3px 0; padding: 2px;"
                 placeholder="默认值：${getDefaultValue('setting_Fast_Remove_Interval_Time')}"
                 value="${GM_getValue('setting_Fast_Remove_Interval_Time')}">
        </div>
        <div style="margin: 0; padding: 0;">
          <p style="margin: 0; padding: 0;">快速删除模式的持续删除时间:</p>
          <input type="text"
                 class="js_setting_input"
                 style="background-color: #808080; margin: 3px 0; padding: 2px;"
                 placeholder="默认值：${getDefaultValue('setting_Fast_Remove_Duration')}"
                 value="${GM_getValue('setting_Fast_Remove_Duration')}">
        </div>
        <div style="margin: 0; padding: 0;">
          <p style="margin: 0; padding: 0;">是否弹出快速输出结束提示框:</p>
          <input type="text"
                 class="js_setting_input"
                 style="background-color: #808080; margin: 3px 0; padding: 2px;"
                 placeholder="默认值：${getDefaultValue('setting_Show_Fast_Remove_Stopped_Alert')}"
                 value="${GM_getValue('setting_Show_Fast_Remove_Stopped_Alert')}">
        </div>
        <p></p>
        <div style="margin: 0; padding: 0;">
          <p style="margin: 0; padding: 0;">是否添加手动清除按钮:</p>
          <input type="text"
                 class="js_setting_input"
                 style="background-color: #808080; margin: 3px 0; padding: 2px;"
                 placeholder="默认值：${getDefaultValue('setting_Add_Manual_Clear_Button')}"
                 value="${GM_getValue('setting_Add_Manual_Clear_Button')}">
        </div>
        <div style="margin: 0; padding: 0;">
          <p style="margin: 0; padding: 0;">设置手动清除按钮加载位置默认值:</p>
          <p style="margin: 0; padding: 0;">(受支持的单位 百分比"%",像素点"px")</p>
          <p style="margin: 0; padding: 0;">按钮的X轴(纵向)值,需要带单位</p>
          <input type="text"
                 class="js_setting_input"
                 id="js_Button_X"
                 style="background-color: #808080; margin: 3px 0; padding: 2px;"
                 placeholder="默认值：${getDefaultValue('setting_Add_Manual_Clear_Button_X')}"
                 value="${Button_X}">
          <p style="margin: 0; padding: 0;">按钮的Y轴(纵向)值,需要带单位</p>
          <input type="text"
                 class="js_setting_input"
                 id="js_Button_Y"
                 style="background-color: #808080; margin: 3px 0; padding: 2px;"
                 placeholder="默认值：${getDefaultValue('setting_Add_Manual_Clear_Button_Y')}"
                 value="${Button_Y}">
        </div>
        <div style="margin: 0; padding: 0;">
          <p style="margin: 0; padding: 0;">在多长时间内快速点击3次移动按钮打开设置:</p>
          <input type="text"
                 class="js_setting_input"
                 style="background-color: #808080; margin: 3px 0; padding: 2px;"
                 placeholder="默认值：${getDefaultValue('setting_Quickly_Click_3Times_To_Open_Settings_Check_Time')}"
                 value="${GM_getValue('setting_Quickly_Click_3Times_To_Open_Settings_Check_Time')}">
        </div>
        <p></p>
        <div style="margin: 0; padding: 0;">
          <p style="margin: 0; padding: 0;">是否打印是否控制台日志</p>
          <input type="text"
                 class="js_setting_input"
                 style="background-color: #808080; margin: 3px 0; padding: 2px;"
                 placeholder="默认值：${getDefaultValue('setting_Print_DebugMode_Log')}"
                 value="${GM_getValue('setting_Print_DebugMode_Log')}">
        </div>
        <p></p>
        <button id="js_setting_reset"
                style="background-color: #808080;">重置为默认值</button>
        <button id="js_setting_save"
                style="background-color: #808080;">保存</button>
    `);

    document.getElementById('js_setting_reset').addEventListener('click', function() {
        value.forEach((event) => {
            // 如果有对应的配置项，则为油猴删除对应的存储配置项
            if (GM_getValue(event.name)) {
                GM_deleteValue(event.name);
            }
            // 注意：这里并没有设置任何值，因为代码只处理删除操作
        });

        // 可以在这里添加其他操作，例如提示用户配置项已被重置
        alert('配置项已被重置！');
    });
    document.getElementById('js_setting_save').addEventListener('click', function() {
        // 选取所有div元素class属性为'js_setting_input'
        let elements = document.getElementsByClassName('js_setting_input');
        // 设置设置框数量
        let number = 10;

        // 检查输入框数量是否正确
        if (elements.length == number) {
            // 循环1到number
            for(let i = 0; i < number; i++) {
                let index = i + 1; // 因为 value 数组第一个元素是版本信息，所以从第二个开始
                // 将字符串转换为布尔值
                let boolValue = (elements[i].value === 'true') ? true : (elements[i].value === 'false') ? false : elements[i].value;
                // 设置数据库 value数组的第 i 个项目名 数据为网页元素的第 i 个内容
                GM_setValue(value[index].name, boolValue); // 存储值，可能是字符串或布尔值
                log(`目前是第 ${index} 个输入框，\n数据库数据名：${value[index].name} 的\n值已经设置为: ${GM_getValue(value[index].name)}`);
            }
        } else {
            console.error("输入框数量不符，实际数量：" + elements.length + ", 预期数量：" + number);
        }

        // 可以在这里添加其他操作，例如提示用户配置项已被重置
        alert('配置项已被保存！');
    });

}

// 简单粗暴直接对所有都添加样式(显示为空)以屏蔽弹窗
//GM_addStyle("div[data-testid='beast-core-modal-mask']{display:none !important}");
//GM_addStyle("div[data-testid='beast-core-modal']{display:none !important}");
// 已弃用

// 提示用户js用户脚本将会等待变量值 GM_getValue('setting_Start_Wait_Time') 毫秒
// alert(`将会等待 ${GM_getValue('setting_Start_Wait_Time') / 1000} 秒后开始执行`);
// 已弃用
insertModalDivs(`<div><h3>来自 拼多多跨境(Temu)弹窗屏蔽 js用户脚本提示：\n</h3><br>将会等待 ${GM_getValue('setting_Start_Wait_Time') / 1000} 秒后开始执行删除</div>`);

setTimeout(function() {
    // insertModalDivs 函数已经定义，并且接收文本参数来插入模态框
    insertModalDivs('<div><h3>来自 拼多多跨境(Temu)弹窗屏蔽 js用户脚本提示：\n</h3><br>开始操作</div>'); // 使用你想要显示的文本作为参数

    setTimeout(function() {
        // 直接在setTimeout回调中调用remove函数
        remove();
    }, 0); // 延迟0毫秒
}, GM_getValue('setting_Start_Wait_Time')); // 延迟GM_getValue('setting_Start_Wait_Time')毫秒

// remove 函数的定义
function remove() {
    // 检查 document.readyState 是否为 'loading'
    if (document.readyState === 'loading') {
        // 如果还在加载中，等待 DOMContentLoaded 事件
        document.addEventListener('DOMContentLoaded', startInterval);
    } else {
        // 如果 DOM 已经加载完成，则直接执行删除操作
        startInterval();
    }
}

// 定义一个变量来存储interval ID，以便稍后清除它
let intervalId;

// 启动interval的函数
function startInterval() {
    // 设置interval来每X毫秒执行removeElements
    intervalId = setInterval(removeElements, GM_getValue('setting_Fast_Remove_Interval_Time')); // 这里的单位是毫秒，你可以根据需要调整

    // 设置timeout来在 GM_getValue('setting_Fast_Remove_Duration') 毫秒后退出执行
    setTimeout(() => {
        clearInterval(intervalId);
        // 检查两个值是否不同，则弹出提示
        if (GM_getValue('setting_Fast_Remove_Interval_Time') !== GM_getValue('setting_Fast_Remove_Duration')) {
            log(`${GM_getValue('setting_Fast_Remove_Duration')} 毫秒已过，停止删除操作。`);
            // 如果GM_getValue('setting_Show_Fast_Remove_Stopped_Alert')为true，则弹出提示框
            if (GM_getValue('setting_Show_Fast_Remove_Stopped_Alert')) {
                alert(`设置的循环时间 ${GM_getValue('setting_Fast_Remove_Duration')} 毫秒到了,您可以继续操作了 `);
            }
        } else {
            log('快速模式未启用'); // 注意：分号应该在括号外面
        }
    }, GM_getValue('setting_Fast_Remove_Duration')); //  这里的单位是毫秒，你可以根据需要调整
}

function removeElements() {
    // 移除具有特定id属性的div元素（js_info）
    const elementsWithTestId0 = document.querySelectorAll('div[id="js_info"]');
    elementsWithTestId0.forEach(element => element.remove());


    // 查找并删除具有特定data-testid属性的div元素
    const elementsWithTestId = document.querySelectorAll('div[data-testid="beast-core-modal"]');

    elementsWithTestId.forEach(element => {
        let shouldRemoveParent = true; // 初始化变量

        // 查找 modalDiv 之前的具有 data-testid="beast-core-modal-mask" 的 div 元素
        let maskDiv = element.previousElementSibling;
        while (maskDiv && !maskDiv.matches('div[data-testid="beast-core-modal-mask"]')) {
            maskDiv = maskDiv.previousElementSibling;
        }

        // 如果GM_getValue('setting_Show_Price_Menu')为false，则直接删除元素，不检查子结构
        if (!GM_getValue('setting_Show_Price_Menu')) {
            element.remove();
            return; // 跳出当前循环，因为已经删除了元素
        }

        // 查找包含“切换店铺”文本的div
        const switchShopDiv = element.querySelector('.layout_title__1eHi_');
        if (switchShopDiv && switchShopDiv.textContent.trim().includes('切换店铺')) {
            shouldRemoveParent = false; // 如果找到，不删除父元素
        }

        // 在当前element中查找所有匹配的<th>元素
        const headers = element.querySelectorAll('.TB_thead_5-109-0 .TB_th_5-109-0');

        headers.forEach(header => {
            // 检查<th>元素的文本内容是否包含"调价原因"
            if (header.textContent.trim().includes('调价原因')) {
                shouldRemoveParent = false; // 不删除或隐藏父元素
                // 如果需要，可以在这里添加其他逻辑，比如修改<th>的样式
                // header.style.display = 'none'; // 隐藏<th>元素（如果需要）
                return; // 跳出内部循环的当前迭代
            }
        });

        // 根据shouldRemoveParent的值执行操作
        if (shouldRemoveParent) {
            log('没有找到包含 "调价原因" 或 "切换店铺" 的元素');
            element.remove();

            // 如果找到了匹配的 maskDiv，则删除它
            if (maskDiv && maskDiv.matches('div[data-testid="beast-core-modal-mask"]')) {
                maskDiv.parentNode.removeChild(maskDiv);
            }

        } else {
            log('找到包含 "调价原因" 或 "切换店铺" 的元素');
        }

        //结束后删除空的div
        setTimeout(function() {
            // 调用函数以删除body尾部的空div
            removeEmptyDivsAtBodyEnd();
        }, GM_getValue('setting_Start_Wait_Time') + GM_getValue('setting_Fast_Remove_Duration') + 10000); // 延迟：毫秒

    });
}

// insertModalDivs 函数的定义（如果它在其他地方没有定义的话）
function insertModalDivs(InputHTML) {
    // 这里是插入模态框的代码
    // 插入模态框 div
    var div = document.createElement('div'); // 创建div元素
    div.id = 'js_info'; // 设置id
    div.align = 'center'; // 设置容器内容全部居中

    // 注入css列表
    div.style.cssText = `
        overflow-y: auto;
        max-height: 90%;
        z-index: 2147483648 !important;
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background-color: #000;
        color: #fff;
        padding: 10px;
        border: 1px solid #ccc;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
    `;

    // 直接注入html
    div.innerHTML = `
        <span style="position: absolute; top: -1px; right: 3px; cursor: pointer;"
              onclick="document.getElementById('js_info').remove()">×</span>
        ${InputHTML}
    `;

    // 添加div到boby外
    document.body.insertAdjacentElement("afterend", div);
}

// 防抖函数实现
function debounce(func, wait) {
    let timeout;
    return function() {
        const context = this;
        const args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(() => func.apply(context, args), wait);
    };
}

//监听切换功能区
// 获取.index-module__menuBox___2aaTA元素，并为其添加click事件监听器
//并且参数传递到debounce进行去重
document.querySelector('.index-module__menuBox___2aaTA').addEventListener('click', debounce(function(event) {
    if (event.target.closest('.index-module__menuBox___2aaTA')) {
        // 检查点击的目标是否是.bg-shell-theme-menu-mms的后代
        var mmsDescendant = event.target.closest('.bg-shell-theme-menu-mms');
        if (mmsDescendant) {
            // 进一步检查点击的目标是否是.index-module__menu___3Wyz- .bg-shell-theme-menu的后代
            // 但不是.bg-shell-theme-menu-mms的直接后代
            var menuDescendant = event.target.closest('.index-module__menu___3Wyz- .bg-shell-theme-menu');
            if (menuDescendant && mmsDescendant !== menuDescendant.parentNode) {
                // 这里表示匹配成功
                log('匹配到.index-module__menu___3Wyz- .bg-shell-theme-menu .bg-shell-theme-menu-mms下的.index-module__menu___3Wyz- .bg-shell-theme-menu的后代元素触发的点击');

                // 可以在这里添加点击事件的处理逻辑
                var longtimelist = [
                    '/goods/product/list',
                    '/main/sale-manage/main'
                ];
                setTimeout(function() {
                    log(`等待 100 毫秒,加载网址进行匹配`);
                    var currentPath = window.location.pathname; // 路径部分
                    let time; //初始化变量
                    if (longtimelist.includes(currentPath)) {
                        time = 1400;
                    } else {
                        time = 960;
                    }//判断是否匹配长时间列表

                    setTimeout(function() {
                        log(`已经等待 ${time} 毫秒,执行删除函数`);
                        // 调用函数以删除body尾部的空div
                        removeElements();
                        setTimeout(function() {
                            CheckWebError();
                        }, 1000); // 延迟：毫秒
                    }, time); // 延迟：毫秒
                }, 100); // 延迟：毫秒
            }
        }
    }
}, 1000));

// 定义删除尾部空div
function removeEmptyDivsAtBodyEnd() {
    // 获取body元素的最后一个子节点
    let lastChild = document.body.lastChild;

    // 循环检查body的最后一个子节点，直到找到非div或遇到空的div
    while (lastChild && lastChild.nodeName.toLowerCase() === 'div') {
        // 检查textContent是否为空字符串（忽略空格和换行符）
        if (!lastChild.textContent.trim()) {
            // 如果为空，则移除它
            lastChild.parentNode.removeChild(lastChild);
            // 更新lastChild为新的最后一个子节点
            lastChild = document.body.lastChild;
        } else {
            // 如果不是空的div，则停止循环（因为已经找到了非空或者非div的节点）
            break;
        }
    }
}

function CheckWebError() {

    const rootDiv = document.getElementById('root');
    if (!rootDiv) {
        log('未找到ID为"root"的div元素');
        return;
    }

    const commentsToFind = [
        '<!--- script https://bstatic.cdnfe.com/static/main/maihuo/static/js/bgb-sc-main/runtime~main.ce42606d.js replaced by import-html-entry --->',
        '<!--- script https://bstatic.cdnfe.com/static/main/maihuo/static/js/bgb-sc-main/48.209360ea.chunk.js replaced by import-html-entry --->',
        '<!--- script https://bstatic.cdnfe.com/static/main/maihuo/static/js/bgb-sc-main/main.a38fe5f9.chunk.js replaced by import-html-entry --->'
    ];

    let allCommentsFound = true;

    // 遍历div[id="root"]下的前三个元素（或所有子元素）
    for (let i = 0; i < Math.min(3, rootDiv.childNodes.length); i++) {
        const childNode = rootDiv.childNodes[i];

        // 检查文本节点或元素节点的文本内容是否包含commentsToFind中的注释
        if (childNode.nodeType === Node.TEXT_NODE || childNode.nodeType === Node.ELEMENT_NODE) {
            if (!commentsToFind.some(comment => childNode.textContent.includes(comment.trim()))) {
                allCommentsFound = false;
                break; // 找到一个不匹配的就可以中断循环
            }
        }

        // 如果需要直接检查注释节点（通常不常见，因为注释不是DOM元素）
        // 需要额外的逻辑来解析文本内容中的注释
        // 这里只是一个示例，实际情况可能更复杂
        else if (childNode.nodeType === Node.COMMENT_NODE) {
            // 检查注释节点的nodeValue是否等于commentsToFind中的注释
            if (!commentsToFind.includes(childNode.nodeValue)) {
                allCommentsFound = false;
                break; // 找到一个不匹配的就可以中断循环
            }
        }
    }

    if (allCommentsFound) {
        log('找到所有定义的错误注释,刷新网页');
        insertModalDivs(`<p>检测到网页错误，为您刷新</p>`);
        location.reload(); // 刷新页面
    } else {
        log('未找到所有定义的错误注释,保持原样');
    }

}

// ==UserScript==
// @name         拼多多跨境(Temu)弹窗屏蔽-测试版
// @version      0.5.4
// @description  用于屏蔽拼多多跨境卖家平台的弹窗
// @author       linying
// @match        *://kuajing.pinduoduo.com/*
// @match        *://seller.kuajingmaihuo.com/*
// @match        *://kuajingboss.com/*
// @match        *://agentseller.temu.com/*
// @exclude      */login*
// @exclude      */settle/site-main*
// @exclude      */questionnaire?surveyId=*
// @exclude      */settle/seller-login?redirectUrl=*
// @exclude      */agentseller*.temu.com/main/authentication?redirectUrl=*
// @exclude      */agentseller*.temu.com/mmsos/online-shipping-result.html*
// @supportURL   https://github.com/linying23333
// @homepage     https://github.com/linying23333
// @run-at       document-idle
// @grant        none
// ==/UserScript==
// @require      http://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js
// @note         (预留的jQuery库，以便未来引用)
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// @grant        GM.setValue
// @grant        GM.getValue
// @grant        GM.deleteValue
// @note         (预留的数据库加载，以便未来引用)
// @icon         来自 https://www.iconfont.cn/ 如果侵犯您的权利请与我沟通
// @note         更新日志&常见问题解决：
// @note         本js用户脚本版权归linying所有,仅供研究学习,禁止以任何形式倒卖

//这些参数只能输入数字，否则会导致错误.
//时间需要根据您的电脑加载速度而定,切勿无脑调低或者调高
//设置执行等待时间
let WaitTime = 0; // 单位毫秒,1秒 = 1000毫秒
//设置为 1 不启用，推荐值为3800
//是否展示调价菜单
let ShowPriceMenu = 1; // 默认为"1",设置为"0"则为不展示

//快速删除间隔时间
let FastIntervalTime = 100; // 单位毫秒,1秒 = 1000毫秒
//快速删除持续删除时间
let FastDuration = 8000; // 单位毫秒,1秒 = 1000毫秒
//两个设置为 1 不启用，推荐值为8000
//是否弹出快速输出结束提示框
let FastShow = 0; // 默认为"0",设置为"1"则为展示

//设置手动清除按钮
let AddButton = true; //是否添加手动清除按钮，默认true(开启),使用false(关闭)
let buttonX = 30; //按钮的Y轴(纵向)百分比值
let buttonY = 92; //按钮的X轴(横向)百分比值
//位置从浏览器屏幕左上角开始计算，X轴+1则向下移动移动百分之1，Y轴+1则向右移动移动百分之1

let debugMode = true; // 控制是否打印日志的开关，默认false(关闭),使用true(开启)

'use strict';

/*
防止代码因其他原因被执行多次
这段代码出自 Via轻插件，作者谷花泰
*/
const key = encodeURIComponent('我 是 _拼多多跨境(Temu)弹窗屏蔽-正式白名单版_：({<校验内容>}).');
if (window[key]) return;
window[key] = true;

// 全局定义是否打印日志调用函数
function log(message) {
    if (debugMode) {
        console.log('来自 拼多多跨境(Temu)弹窗屏蔽 js用户脚本提示：\n' + message);
    }
}

// 提示调试模式是否开启
(function() {
    log(`您的 Debug 模式值为 ${debugMode} ,调试模式打印日志已经启用`)
})();

//按钮处理部分
//检查是否启用该部分
if (AddButton) {

    (function() {
        // 创建一个新的按钮元素
        var button = document.createElement('button');
        button.id = 'js_button';
        button.textContent = '清除弹窗!';

        button.style.cssText = `
            z-index: 2147483648 !important;
            position: fixed;
            top: ${buttonY}%;
            left: ${buttonX}%;
            cursor: pointer; /* 鼠标悬停时显示手型 */
        `; // 使用了变量

        // 为按钮添加点击事件监听器
        button.onclick = function() {
            log('手动清除按钮被点击');
            removeElements();
            alert('已经执行清除');
        };

        // 将按钮添加到<body>外
        document.body.insertAdjacentElement("afterend", button);
    })();
}



// 简单粗暴直接对所有都添加样式(显示为空)以屏蔽弹窗
//GM_addStyle("div[data-testid='beast-core-modal-mask']{display:none !important}");
//GM_addStyle("div[data-testid='beast-core-modal']{display:none !important}");
// 已弃用

// 提示用户js用户脚本将会等待变量值 WaitTime 毫秒
// alert(`将会等待 ${WaitTime / 1000} 秒后开始执行`);
// 已弃用
insertModalDivs(`将会等待 ${WaitTime / 1000} 秒后开始执行删除`);

setTimeout(function() {
    // insertModalDivs 函数已经定义，并且接收文本参数来插入模态框
    insertModalDivs('开始操作'); // 使用你想要显示的文本作为参数

    setTimeout(function() {
        // 直接在setTimeout回调中调用remove函数
        remove();
    }, 0); // 延迟0毫秒
}, WaitTime); // 延迟WaitTime毫秒

// remove 函数的定义
function remove() {
    // 检查 document.readyState 是否为 'loading'
    if (document.readyState === 'loading') {
        // 如果还在加载中，等待 DOMContentLoaded 事件
        document.addEventListener('DOMContentLoaded', startInterval);
    } else {
        // 如果 DOM 已经加载完成，则直接执行删除操作
        startInterval();
    }
}

// 定义一个变量来存储interval ID，以便稍后清除它
let intervalId;

// 启动interval的函数
function startInterval() {
    // 设置interval来每X毫秒执行removeElements
    intervalId = setInterval(removeElements, FastIntervalTime); // 这里的单位是毫秒，你可以根据需要调整

    // 设置timeout来在 FastDuration 毫秒后退出执行
    setTimeout(() => {
        clearInterval(intervalId);
        // 检查两个值是否不同，则弹出提示
        if (FastIntervalTime !== FastDuration) {
            log(`${FastDuration} 毫秒已过，停止删除操作。`);
            // 如果FastShow为1，则弹出提示框
            if (FastShow === 1) {
                alert(`设置的循环时间 ${FastDuration} 毫秒到了,您可以继续操作了 `);
            }
        } else {
            log('快速模式未启用'); // 注意：分号应该在括号外面
        }
    }, FastDuration); //  这里的单位是毫秒，你可以根据需要调整
}

function removeElements() {
    // 移除具有特定id属性的div元素（js_info）
    const elementsWithTestId0 = document.querySelectorAll('div[id="js_info"]');
    elementsWithTestId0.forEach(element => element.remove());


    // 查找并删除具有特定data-testid属性的div元素
    const elementsWithTestId = document.querySelectorAll('div[data-testid="beast-core-modal"]');

    elementsWithTestId.forEach(element => {
        let shouldRemoveParent = true; // 初始化变量

        // 查找 modalDiv 之前的具有 data-testid="beast-core-modal-mask" 的 div 元素
        let maskDiv = element.previousElementSibling;
        while (maskDiv && !maskDiv.matches('div[data-testid="beast-core-modal-mask"]')) {
            maskDiv = maskDiv.previousElementSibling;
        }

        // 如果ShowPriceMenu为0，则直接删除元素，不检查子结构
        if (ShowPriceMenu === 0) {
            element.remove();
            return; // 跳出当前循环，因为已经删除了元素
        }

        // 查找包含“切换店铺”文本的div
        const switchShopDiv = element.querySelector('.layout_title__1eHi_');
        if (switchShopDiv && switchShopDiv.textContent.trim().includes('切换店铺')) {
            shouldRemoveParent = false; // 如果找到，不删除父元素
        }

        // 在当前element中查找所有匹配的<th>元素
        const headers = element.querySelectorAll('.TB_thead_5-109-0 .TB_th_5-109-0');

        headers.forEach(header => {
            // 检查<th>元素的文本内容是否包含"调价原因"
            if (header.textContent.trim().includes('调价原因')) {
                shouldRemoveParent = false; // 不删除或隐藏父元素
                // 如果需要，可以在这里添加其他逻辑，比如修改<th>的样式
                // header.style.display = 'none'; // 隐藏<th>元素（如果需要）
                return; // 跳出内部循环的当前迭代
            }
        });

        // 根据shouldRemoveParent的值执行操作
        if (shouldRemoveParent) {
            log('没有找到包含 "调价原因" 或 "切换店铺" 的元素');
            element.remove();

            // 如果找到了匹配的 maskDiv，则删除它
            if (maskDiv && maskDiv.matches('div[data-testid="beast-core-modal-mask"]')) {
                maskDiv.parentNode.removeChild(maskDiv);
            }

        } else {
            log('找到包含 "调价原因" 或 "切换店铺" 的元素');
        }

        //结束后删除空的div
        setTimeout(function() {
            // 调用函数以删除body尾部的空div
            removeEmptyDivsAtBodyEnd();
        }, WaitTime + FastDuration + 10000); // 延迟：毫秒

    });
}

// insertModalDivs 函数的定义（如果它在其他地方没有定义的话）
function insertModalDivs(text) {
    // 这里是插入模态框的代码
    // 插入遮罩层 div
    const newDiv0 = document.createElement('div'); //创建div元素
    newDiv0.id = 'js_info'; // 设置id为js_info
    newDiv0.setAttribute('data-testid', 'beast-core-modal-mask'); //设置属性值"data-testid"的值为"beast-core-modal-mask"
    newDiv0.setAttribute('class', 'MDL_mask_5-109-0');
    newDiv0.setAttribute('style', 'z-index: 2147483647 !important; position: fixed; top: 0; left: 0; right: 0; bottom: 0; background-color: rgba(0, 0, 0, 0.5); /* 示例背景色，可以根据需要调整 */');

    // 将遮罩层 div 添加到 body 的末尾
    document.body.appendChild(newDiv0);

    // 插入模态框 div
    const newDiv = document.createElement('div');
    newDiv.id = 'js_info'; // 设置id为js_info
    newDiv.setAttribute('data-testid', 'beast-core-modal');
    newDiv.setAttribute('class', 'MDL_outerWrapper_5-109-0 MDL_alert_5-109-0 undefined');
    newDiv.setAttribute('style', 'z-index: 2147483647 !important; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%);'); // 居中显示

    // 设置模态框的内容
    newDiv.innerHTML = `
        <div data-testid="beast-core-modal-container" class="MDL_container_5-109-0">
            <div data-testid="beast-core-modal-innerWrapper" class="MDL_innerWrapper_5-109-0" style="z-index: 2147483647 !important;">
                <div data-testid="beast-core-modal-inner" class="MDL_inner_5-109-0" style="min-width: 50px; max-width: 1000px;">
                    <div data-testid="beast-core-modal-body" class="MDL_body_5-109-0 MDL_noHeader_5-109-0">
                        <div data-testid="beast-core-box">
                            <div class="modal_title"><h1>提示:</h1></div></div><br>
                        <div class="modal_body_content">
                            来自 拼多多跨境(Temu)弹窗屏蔽 js用户脚本提示：\n
                            <br>
                            ${text} <!-- 这里插入传入的 text 变量 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `;
    //网页结构复用拼多多中心消息提示

    // 将模态框 div 添加到 body 的末尾（通常在遮罩层之后）
    document.body.appendChild(newDiv);
}

// 防抖函数实现
function debounce(func, wait) {
    let timeout;
    return function() {
        const context = this;
        const args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(() => func.apply(context, args), wait);
    };
}

//监听切换功能区
// 获取.index-module__menuBox___2aaTA元素，并为其添加click事件监听器
//并且参数传递到debounce进行去重
document.querySelector('.index-module__menuBox___2aaTA').addEventListener('click', debounce(function(event) {
    if (event.target.closest('.index-module__menuBox___2aaTA')) {
        // 检查点击的目标是否是.bg-shell-theme-menu-mms的后代
        var mmsDescendant = event.target.closest('.bg-shell-theme-menu-mms');
        if (mmsDescendant) {
            // 进一步检查点击的目标是否是.index-module__menu___3Wyz- .bg-shell-theme-menu的后代
            // 但不是.bg-shell-theme-menu-mms的直接后代
            var menuDescendant = event.target.closest('.index-module__menu___3Wyz- .bg-shell-theme-menu');
            if (menuDescendant && mmsDescendant !== menuDescendant.parentNode) {
                // 这里表示匹配成功
                log('匹配到.index-module__menu___3Wyz- .bg-shell-theme-menu .bg-shell-theme-menu-mms下的.index-module__menu___3Wyz- .bg-shell-theme-menu的后代元素触发的点击');

                // 可以在这里添加点击事件的处理逻辑
                var longtimelist = [
                    '/goods/product/list',
                    '/main/sale-manage/main'
                ];
                setTimeout(function() {
                    log(`等待 100 毫秒,加载网址进行匹配`);
                    var currentPath = window.location.pathname; // 路径部分
                    let time; //初始化变量
                    if (longtimelist.includes(currentPath)) {
                        time = 1200;
                    } else {
                        time = 760;
                    }//判断是否匹配长时间列表

                    setTimeout(function() {
                        log(`已经等待 ${time} 毫秒,执行删除函数`);
                        // 调用函数以删除body尾部的空div
                        removeElements();
                        setTimeout(function() {
                            CheckWebError();
                        }, 1000); // 延迟：毫秒
                    }, time); // 延迟：毫秒
                }, 100); // 延迟：毫秒
            }
        }
    }
}, 1000));

// 定义删除尾部空div
function removeEmptyDivsAtBodyEnd() {
    // 获取body元素的最后一个子节点
    let lastChild = document.body.lastChild;

    // 循环检查body的最后一个子节点，直到找到非div或遇到空的div
    while (lastChild && lastChild.nodeName.toLowerCase() === 'div') {
        // 检查textContent是否为空字符串（忽略空格和换行符）
        if (!lastChild.textContent.trim()) {
            // 如果为空，则移除它
            lastChild.parentNode.removeChild(lastChild);
            // 更新lastChild为新的最后一个子节点
            lastChild = document.body.lastChild;
        } else {
            // 如果不是空的div，则停止循环（因为已经找到了非空或者非div的节点）
            break;
        }
    }
}

function CheckWebError() {

const commentsToFind = [
    '<!--- script https://bstatic.cdnfe.com/static/main/maihuo/static/js/bgb-sc-main/runtime~main.ce42606d.js replaced by import-html-entry --->',
    '<!--- script https://bstatic.cdnfe.com/static/main/maihuo/static/js/bgb-sc-main/48.209360ea.chunk.js replaced by import-html-entry --->',
    '<!--- script https://bstatic.cdnfe.com/static/main/maihuo/static/js/bgb-sc-main/main.a38fe5f9.chunk.js replaced by import-html-entry --->'
];  
  
// 获取div[id="root"]下的所有子元素  
const rootDiv = document.getElementById('root');  
const children = Array.from(rootDiv.children);  
  
// 检查前三个子元素是否都是commentsToFind中的注释  
function areFirstThreeCommentsInList(children, commentsToFind) {  
    for (let i = 0; i < 3 && i < children.length; i++) {  
        const child = children[i];  
        // 检查元素是否是注释节点  
        if (!(child.nodeType === Node.COMMENT_NODE && commentsToFind.includes(child.nodeValue.trim()))) {  
            return false;  
        }  
    }  
    return true;  
}  
  
// 检查并决定是否刷新页面  
if (areFirstThreeCommentsInList(children, commentsToFind)) {  
    location.reload(); // 刷新页面  
}

}

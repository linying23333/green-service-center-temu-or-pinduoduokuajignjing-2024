// ==UserScript==
// @name         拼多多跨境(Temu)弹窗屏蔽
// @version      0.5.6.1
// @description  用于屏蔽拼多多跨境卖家平台的弹窗
// @author       linying
// @match        *://kuajing.pinduoduo.com/*
// @match        *://seller.kuajingmaihuo.com/*
// @match        *://kuajingboss.com/*
// @match        *://agentseller.temu.com/*
// @exclude      */login*
// @exclude      */settle/site-main*
// @exclude      */questionnaire?surveyId=*
// @exclude      */settle/seller-login?redirectUrl=*
// @exclude      */agentseller*.temu.com/main/authentication?redirectUrl=*
// @exclude      */agentseller*.temu.com/mmsos/online-shipping-result.html*
// @icon         data:image/svg+xml;charset=utf-8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+PHN2ZyB0PSIxNzE2MzU2OTE5NTgzIiBjbGFzcz0iaWNvbiIgdmlld0JveD0iMCAwIDEwMjQgMTAyNCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHAtaWQ9IjM2NDkiIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48cGF0aCBkPSJNODYwLjA2ODU3MSA2NjYuNDIyODU3TDcyNi44NTcxNDMgNzMxLjI0NTcxNGMtOC42ODU3MTQgNC4yMDU3MTQtMTguNzQyODU3IDQuMjA1NzE0LTI3LjQyODU3MiAwbC0xMzMuMjExNDI4LTY0LjgyMjg1N0EzMS4yOTYgMzEuMjk2IDAgMCAxIDU0OC41NzE0MjkgNjM4LjI2Mjg1N1Y0NzcuMTY1NzE0YzAtMTEuOTc3MTQzIDYuODU3MTQzLTIyLjk0ODU3MSAxNy42NDU3MTQtMjguMTZsMTMzLjIxMTQyOC02NC44MjI4NTdjOC42ODU3MTQtNC4yMDU3MTQgMTguNzQyODU3LTQuMjA1NzE0IDI3LjQyODU3MiAwbDEzMy4yMTE0MjggNjQuODIyODU3YzEwLjc4ODU3MSA1LjIxMTQyOSAxNy42NDU3MTQgMTYuMTgyODU3IDE3LjY0NTcxNSAyOC4xNnYxNjEuMDk3MTQzYzAgMTEuOTc3MTQzLTYuODU3MTQzIDIyLjk0ODU3MS0xNy42NDU3MTUgMjguMTZ6IiBmaWxsPSIjQjREN0ZFIiBwLWlkPSIzNjUwIj48L3BhdGg+PHBhdGggZD0iTTcxMy4xNDI4NTcgNzAxLjk4ODU3MVY1NDMuNTQyODU3bC0xMzkuMzM3MTQzLTY4LjU3MTQyOE04NTIuNDggNDc0Ljk3MTQyOUw3MTMuMTQyODU3IDU0My41NDI4NTciIGZpbGw9IiNCNEQ3RkUiIHAtaWQ9IjM2NTEiPjwvcGF0aD48cGF0aCBkPSJNOTUwLjg1NzE0MyAxNDYuMjg1NzE0SDczLjE0Mjg1N2MtMTAuMDU3MTQzIDAtMTguMjg1NzE0IDguMjI4NTcxLTE4LjI4NTcxNCAxOC4yODU3MTV2NjIxLjcxNDI4NWMwIDEwLjA1NzE0MyA4LjIyODU3MSAxOC4yODU3MTQgMTguMjg1NzE0IDE4LjI4NTcxNWgyMDUuNzE0Mjg2djU0Ljg1NzE0MmMwIDEwLjA1NzE0MyA4LjIyODU3MSAxOC4yODU3MTQgMTguMjg1NzE0IDE4LjI4NTcxNWg0MjkuNzE0Mjg2YzEwLjA1NzE0MyAwIDE4LjI4NTcxNC04LjIyODU3MSAxOC4yODU3MTQtMTguMjg1NzE1di01NC44NTcxNDJIOTUwLjg1NzE0M2MxMC4wNTcxNDMgMCAxOC4yODU3MTQtOC4yMjg1NzEgMTguMjg1NzE0LTE4LjI4NTcxNVYxNjQuNTcxNDI5YzAtMTAuMDU3MTQzLTguMjI4NTcxLTE4LjI4NTcxNC0xOC4yODU3MTQtMTguMjg1NzE1ek03MDguNTcxNDI5IDg0MS4xNDI4NTdoLTM5My4xNDI4NTh2LTM2LjU3MTQyOGgzOTMuMTQyODU4djM2LjU3MTQyOHogbTIyNC03My4xNDI4NTdIOTEuNDI4NTcxVjE4Mi44NTcxNDNoODQxLjE0Mjg1OHY1ODUuMTQyODU3eiIgZmlsbD0iIzI0OEJGRiIgcC1pZD0iMzY1MiI+PC9wYXRoPjxwYXRoIGQ9Ik0xODIuODU3MTQzIDI3NC4yODU3MTRoMjAxLjE0Mjg1N3YzNi41NzE0MjlIMTgyLjg1NzE0M3oiIGZpbGw9IiNCNEQ3RkUiIHAtaWQ9IjM2NTMiPjwvcGF0aD48cGF0aCBkPSJNNDExLjQyODU3MSAyMzcuNzE0Mjg2SDE1NS40Mjg1NzFjLTUuMDI4NTcxIDAtOS4xNDI4NTcgNC4xMTQyODYtOS4xNDI4NTcgOS4xNDI4NTd2NDU3LjE0Mjg1N2MwIDUuMDI4NTcxIDQuMTE0Mjg2IDkuMTQyODU3IDkuMTQyODU3IDkuMTQyODU3aDI1NmM1LjAyODU3MSAwIDkuMTQyODU3LTQuMTE0Mjg2IDkuMTQyODU4LTkuMTQyODU3VjI0Ni44NTcxNDNjMC01LjAyODU3MS00LjExNDI4Ni05LjE0Mjg1Ny05LjE0Mjg1OC05LjE0Mjg1N3ogbS0yMjguNTcxNDI4IDM2LjU3MTQyOGgyMDEuMTQyODU3djM2LjU3MTQyOUgxODIuODU3MTQzdi0zNi41NzE0Mjl6IG0wIDQwMi4yODU3MTVWMzQ3LjQyODU3MWgyMDEuMTQyODU3djMyOS4xNDI4NThIMTgyLjg1NzE0M3pNODY3LjQ3NDI4NiA0NTIuNTcxNDI5bC0xNDYuMjg1NzE1LTcxLjIyMjg1OGMtMi41Ni0xLjE4ODU3MS01LjMwMjg1Ny0xLjgyODU3MS04LjA0NTcxNC0xLjgyODU3MXMtNS40ODU3MTQgMC42NC04LjA0NTcxNCAxLjgyODU3MWwtMTQ2LjI4NTcxNCA3MS4yMjI4NThjLTYuMjE3MTQzIDMuMTA4NTcxLTEwLjI0IDkuNDE3MTQzLTEwLjI0IDE2LjQ1NzE0MnYxNzcuMzcxNDI5YzAgNy4wNCA0LjAyMjg1NyAxMy4zNDg1NzEgMTAuMjQgMTYuNDU3MTQzbDE0Ni4yODU3MTQgNzEuMjIyODU3YzIuNTYgMS4xODg1NzEgNS4zMDI4NTcgMS44Mjg1NzEgOC4wNDU3MTQgMS44Mjg1NzFzNS40ODU3MTQtMC42NCA4LjA0NTcxNC0xLjgyODU3MWwxNDYuMjg1NzE1LTcxLjIyMjg1N2M2LjMwODU3MS0zLjAxNzE0MyAxMC4yNC05LjQxNzE0MyAxMC4yNC0xNi40NTcxNDNWNDY5LjAyODU3MWMwLTcuMDQtNC4wMjI4NTctMTMuMzQ4NTcxLTEwLjI0LTE2LjQ1NzE0MnpNNzEzLjE0Mjg1NyA0MTguMTAyODU3bDEwNy4yNDU3MTQgNTIuMjA1NzE0LTEwNy40Mjg1NzEgNTIuOTM3MTQzYy0wLjY0LTAuMzY1NzE0LTEuMjgtMC44MjI4NTctMS45Mi0xLjA5NzE0M2wtMTA1LjE0Mjg1Ny01MS43NDg1NzFMNzEzLjE0Mjg1NyA0MTguMTAyODU3eiBtLTEyOCA4Mi44MzQyODZsMTA5LjcxNDI4NiA1NC4wMzQyODZ2MTMzLjM5NDI4NWwtMTA5LjcxNDI4Ni01My4zOTQyODVWNTAwLjkzNzE0M3ogbTE0Ni4yODU3MTQgMTg3LjQyODU3MVY1NTQuOTcxNDI5bDEwOS43MTQyODYtNTQuMDM0Mjg2djEzNC4wMzQyODZsLTEwOS43MTQyODYgNTMuMzk0Mjg1eiIgZmlsbD0iIzI0OEJGRiIgcC1pZD0iMzY1NCI+PC9wYXRoPjxwYXRoIGQ9Ik0zNDcuNDI4NTcxIDQxMS40Mjg1NzFIMjE5LjQyODU3MWMtMTAuMDU3MTQzIDAtMTguMjg1NzE0LTguMjI4NTcxLTE4LjI4NTcxNC0xOC4yODU3MTRzOC4yMjg1NzEtMTguMjg1NzE0IDE4LjI4NTcxNC0xOC4yODU3MTRoMTI4YzEwLjA1NzE0MyAwIDE4LjI4NTcxNCA4LjIyODU3MSAxOC4yODU3MTUgMTguMjg1NzE0cy04LjIyODU3MSAxOC4yODU3MTQtMTguMjg1NzE1IDE4LjI4NTcxNHpNODU5LjQyODU3MSAyOTIuNTcxNDI5SDQ4NC41NzE0MjljLTEwLjA1NzE0MyAwLTE4LjI4NTcxNC04LjIyODU3MS0xOC4yODU3MTUtMTguMjg1NzE1czguMjI4NTcxLTE4LjI4NTcxNCAxOC4yODU3MTUtMTguMjg1NzE0aDM3NC44NTcxNDJjMTAuMDU3MTQzIDAgMTguMjg1NzE0IDguMjI4NTcxIDE4LjI4NTcxNSAxOC4yODU3MTRzLTguMjI4NTcxIDE4LjI4NTcxNC0xOC4yODU3MTUgMTguMjg1NzE1ek02NjcuNDI4NTcxIDM1Ni41NzE0MjlINDg0LjU3MTQyOWMtMTAuMDU3MTQzIDAtMTguMjg1NzE0LTguMjI4NTcxLTE4LjI4NTcxNS0xOC4yODU3MTVzOC4yMjg1NzEtMTguMjg1NzE0IDE4LjI4NTcxNS0xOC4yODU3MTRoMTgyLjg1NzE0MmMxMC4wNTcxNDMgMCAxOC4yODU3MTQgOC4yMjg1NzEgMTguMjg1NzE1IDE4LjI4NTcxNHMtOC4yMjg1NzEgMTguMjg1NzE0LTE4LjI4NTcxNSAxOC4yODU3MTV6TTU0OC41NzE0MjkgNDIwLjU3MTQyOWgtNjRjLTEwLjA1NzE0MyAwLTE4LjI4NTcxNC04LjIyODU3MS0xOC4yODU3MTUtMTguMjg1NzE1czguMjI4NTcxLTE4LjI4NTcxNCAxOC4yODU3MTUtMTguMjg1NzE0aDY0YzEwLjA1NzE0MyAwIDE4LjI4NTcxNCA4LjIyODU3MSAxOC4yODU3MTQgMTguMjg1NzE0cy04LjIyODU3MSAxOC4yODU3MTQtMTguMjg1NzE0IDE4LjI4NTcxNXpNMjM3LjcxNDI4NiA0NzUuNDI4NTcxaC0xOC4yODU3MTVjLTEwLjA1NzE0MyAwLTE4LjI4NTcxNC04LjIyODU3MS0xOC4yODU3MTQtMTguMjg1NzE0czguMjI4NTcxLTE4LjI4NTcxNCAxOC4yODU3MTQtMTguMjg1NzE0aDE4LjI4NTcxNWMxMC4wNTcxNDMgMCAxOC4yODU3MTQgOC4yMjg1NzEgMTguMjg1NzE0IDE4LjI4NTcxNHMtOC4yMjg1NzEgMTguMjg1NzE0LTE4LjI4NTcxNCAxOC4yODU3MTR6IiBmaWxsPSIjMjQ4QkZGIiBwLWlkPSIzNjU1Ij48L3BhdGg+PHBhdGggZD0iTTMwMS43MTQyODYgNTM5LjQyODU3MWgtODIuMjg1NzE1Yy0xMC4wNTcxNDMgMC0xOC4yODU3MTQtOC4yMjg1NzEtMTguMjg1NzE0LTE4LjI4NTcxNHM4LjIyODU3MS0xOC4yODU3MTQgMTguMjg1NzE0LTE4LjI4NTcxNGg4Mi4yODU3MTVjMTAuMDU3MTQzIDAgMTguMjg1NzE0IDguMjI4NTcxIDE4LjI4NTcxNCAxOC4yODU3MTRzLTguMjI4NTcxIDE4LjI4NTcxNC0xOC4yODU3MTQgMTguMjg1NzE0eiIgZmlsbD0iIzI0OEJGRiIgcC1pZD0iMzY1NiI+PC9wYXRoPjxwYXRoIGQ9Ik0yNzQuMjg1NzE0IDYwMy40Mjg1NzFoLTU0Ljg1NzE0M2MtMTAuMDU3MTQzIDAtMTguMjg1NzE0LTguMjI4NTcxLTE4LjI4NTcxNC0xOC4yODU3MTRzOC4yMjg1NzEtMTguMjg1NzE0IDE4LjI4NTcxNC0xOC4yODU3MTRoNTQuODU3MTQzYzEwLjA1NzE0MyAwIDE4LjI4NTcxNCA4LjIyODU3MSAxOC4yODU3MTUgMTguMjg1NzE0cy04LjIyODU3MSAxOC4yODU3MTQtMTguMjg1NzE1IDE4LjI4NTcxNHoiIGZpbGw9IiMyNDhCRkYiIHAtaWQ9IjM2NTciPjwvcGF0aD48L3N2Zz48IS0tIOWOn+aWh+S7tuWQje+8miLnu4TmgIHlt6Xlhbcuc3ZnIiAtLT4=
// @supportURL   https://gitlab.com/linying23333/green-service-center-temu-or-pinduoduokuajing-2024
// @homepage     https://github.com/linying2333
// @run-at       document-idle
// @grant        none
// ==/UserScript==
// @require      http://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js
// @note         (预留的jQuery库，以便未来引用)
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// @grant        GM.setValue
// @grant        GM.getValue
// @grant        GM.deleteValue
// @note         (预留的数据库加载，以便未来引用)
// @icon         来自 https://www.iconfont.cn/ 如果侵犯您的权利请与我沟通
// @note         更新日志&常见问题解决：https://greasyfork.org/zh-CN/scripts/496221
// @note         本js用户脚本版权归linying所有,仅供研究学习,禁止以任何形式倒卖

//这些参数只能输入数字，否则会导致错误.
//时间需要根据您的电脑加载速度而定,切勿无脑调低或者调高
//设置执行等待时间
let WaitTime = 4200; // 单位毫秒,1秒 = 1000毫秒
//设置为 1 不启用，推荐值为3800
//是否展示调价菜单
let ShowPriceMenu = true; // 默认true(开启),使用false(关闭)

//快速删除间隔时间
let FastIntervalTime = 1; // 单位毫秒,1秒 = 1000毫秒
//快速删除持续删除时间
let FastDuration = 1; // 单位毫秒,1秒 = 1000毫秒
//两个设置为 1 不启用，推荐值为8000
//是否弹出快速输出结束提示框
let FastShow = false; // 默认false(关闭),使用true(开启)

//设置手动清除按钮
let AddButton = true; //是否添加手动清除按钮，默认true(开启),使用false(关闭)
//设置加载默认值
let buttonX = "30%"; //按钮的Y轴(纵向)值,带单位(受支持的单位 百分比"%",像素点"px")
let buttonY = "92%"; //按钮的X轴(横向)值,带单位(受支持的单位 百分比"%",像素点"px")
//位置从浏览器屏幕左上角开始计算，X轴+1则向右移动，Y轴+1则向下移动

let debugMode = false; // 控制是否打印日志的开关，默认false(关闭),使用true(开启)

'use strict';

/*
防止代码因其他原因被执行多次
这段代码出自 Via轻插件，作者谷花泰
*/
const key = encodeURIComponent('拼多多跨境(Temu)弹窗屏蔽');
if (window[key]) return;
window[key] = true;

// 全局定义是否打印日志调用函数
function log(message) {
    if (debugMode) {
        console.log('来自 拼多多跨境(Temu)弹窗屏蔽 js用户脚本提示：\n' + message);
    }
}

// 提示调试模式是否开启
(function() {
    log(`您的 Debug 模式值为 ${debugMode} ,调试模式打印日志已经启用`)
})();

// 按钮处理部分
// 检查是否启用该部分
if (AddButton) {
    (function() {
        // 创建新的div
        var div = document.createElement('div');
        div.id = 'js_button_div'; // 设置id

        //禁止选择文字
        div.style.userSelect = 'none'; // 对于Webkit和Mozilla浏览器，IE浏览器应使用'unselectable'
        div.style.MozUserSelect = 'none'; // 对于早期的Firefox
        div.style.webkitUserSelect = 'none'; // 对于早期的Chrome和Safari
        div.style.msUserSelect = 'none'; // IE 10+

        div.style.cssText = `
          z-index: 2147483648 !important;
          position: fixed;
          top: ${buttonY};
          left: ${buttonX};
        `; // 使用了变量

        // 添加div到boby外
        document.body.insertAdjacentElement("afterend", div);

        // 创建移动按钮元素
        var eventMoveButton = document.createElement('button');
        eventMoveButton.id = 'js_MoveButton';
        eventMoveButton.style.cssText = ` cursor: move; /* 切换用户鼠标为移动图标 */ `;
        eventMoveButton.textContent = '✣';
        document.getElementById('js_button_div').appendChild(eventMoveButton); // 确保按钮添加到div后才操作

        // 创建一个新的按钮元素
        var CleanButton = document.createElement('button');
        CleanButton.id = 'js_CleanButton';
        CleanButton.textContent = '清除弹窗!';

        CleanButton.style.cssText = ` cursor: pointer; /* 鼠标悬停时显示手型 */ `;

        // 为按钮添加点击事件监听器
        CleanButton.onclick = function() {
            log('手动清除按钮被点击');
            removeElements();
            alert('已经执行清除');
        };

        document.getElementById('js_button_div').appendChild(CleanButton); // 确保清除按钮添加到div中

        // 初始化按钮功能
        var isDraggable = false; // 初始化拖动状态为禁用
        var eventDiv = document.getElementById('js_button_div');
            // 拖动逻辑
            document.getElementById('js_MoveButton').addEventListener('mousedown', function(mouseDownEvent) {
                if (!isDraggable) return; // 如果不可拖动，则不执行拖动逻辑
                function onMouseMove(mouseMoveEvent) {
                    var newX = mouseMoveEvent.clientX;
                    var newY = mouseMoveEvent.clientY;
                    eventDiv.style.left = newX + 'px';
                    eventDiv.style.top = newY + 'px';
                }

                document.addEventListener('mousemove', onMouseMove);
                document.addEventListener('mouseup', function() {
                    document.removeEventListener('mousemove', onMouseMove);
                    log("最终位置：(X: " + eventDiv.style.left + ", Y: " + eventDiv.style.top + ")");
                }, {once: true});

            });

            // 切换拖动状态的点击事件
            document.getElementById('js_MoveButton').addEventListener('click', function() {
                isDraggable = !isDraggable;
                this.textContent = isDraggable ? '↻' : '✣';
            });
    })();
}


// 简单粗暴直接对所有都添加样式(显示为空)以屏蔽弹窗
//GM_addStyle("div[data-testid='beast-core-modal-mask']{display:none !important}");
//GM_addStyle("div[data-testid='beast-core-modal']{display:none !important}");
// 已弃用

// 提示用户js用户脚本将会等待变量值 WaitTime 毫秒
// alert(`将会等待 ${WaitTime / 1000} 秒后开始执行`);
// 已弃用
insertModalDivs(`将会等待 ${WaitTime / 1000} 秒后开始执行删除`);

setTimeout(function() {
    // insertModalDivs 函数已经定义，并且接收文本参数来插入模态框
    insertModalDivs('开始操作'); // 使用你想要显示的文本作为参数

    setTimeout(function() {
        // 直接在setTimeout回调中调用remove函数
        remove();
    }, 0); // 延迟0毫秒
}, WaitTime); // 延迟WaitTime毫秒

// remove 函数的定义
function remove() {
    // 检查 document.readyState 是否为 'loading'
    if (document.readyState === 'loading') {
        // 如果还在加载中，等待 DOMContentLoaded 事件
        document.addEventListener('DOMContentLoaded', startInterval);
    } else {
        // 如果 DOM 已经加载完成，则直接执行删除操作
        startInterval();
    }
}

// 定义一个变量来存储interval ID，以便稍后清除它
let intervalId;

// 启动interval的函数
function startInterval() {
    // 设置interval来每X毫秒执行removeElements
    intervalId = setInterval(removeElements, FastIntervalTime); // 这里的单位是毫秒，你可以根据需要调整

    // 设置timeout来在 FastDuration 毫秒后退出执行
    setTimeout(() => {
        clearInterval(intervalId);
        // 检查两个值是否不同，则弹出提示
        if (FastIntervalTime !== FastDuration) {
            log(`${FastDuration} 毫秒已过，停止删除操作。`);
            // 如果FastShow为true，则弹出提示框
            if (FastShow) {
                alert(`设置的循环时间 ${FastDuration} 毫秒到了,您可以继续操作了 `);
            }
        } else {
            log('快速模式未启用'); // 注意：分号应该在括号外面
        }
    }, FastDuration); //  这里的单位是毫秒，你可以根据需要调整
}

function removeElements() {
    // 移除具有特定id属性的div元素（js_info）
    const elementsWithTestId0 = document.querySelectorAll('div[id="js_info"]');
    elementsWithTestId0.forEach(element => element.remove());


    // 查找并删除具有特定data-testid属性的div元素
    const elementsWithTestId = document.querySelectorAll('div[data-testid="beast-core-modal"]');

    elementsWithTestId.forEach(element => {
        let shouldRemoveParent = true; // 初始化变量

        // 查找 modalDiv 之前的具有 data-testid="beast-core-modal-mask" 的 div 元素
        let maskDiv = element.previousElementSibling;
        while (maskDiv && !maskDiv.matches('div[data-testid="beast-core-modal-mask"]')) {
            maskDiv = maskDiv.previousElementSibling;
        }

        // 如果ShowPriceMenu为false，则直接删除元素，不检查子结构
        if (!ShowPriceMenu) {
            element.remove();
            return; // 跳出当前循环，因为已经删除了元素
        }

        // 查找包含“切换店铺”文本的div
        const switchShopDiv = element.querySelector('.layout_title__1eHi_');
        if (switchShopDiv && switchShopDiv.textContent.trim().includes('切换店铺')) {
            shouldRemoveParent = false; // 如果找到，不删除父元素
        }

        // 在当前element中查找所有匹配的<th>元素
        const headers = element.querySelectorAll('.TB_thead_5-109-0 .TB_th_5-109-0');

        headers.forEach(header => {
            // 检查<th>元素的文本内容是否包含"调价原因"
            if (header.textContent.trim().includes('调价原因')) {
                shouldRemoveParent = false; // 不删除或隐藏父元素
                // 如果需要，可以在这里添加其他逻辑，比如修改<th>的样式
                // header.style.display = 'none'; // 隐藏<th>元素（如果需要）
                return; // 跳出内部循环的当前迭代
            }
        });

        // 根据shouldRemoveParent的值执行操作
        if (shouldRemoveParent) {
            log('没有找到包含 "调价原因" 或 "切换店铺" 的元素');
            element.remove();

            // 如果找到了匹配的 maskDiv，则删除它
            if (maskDiv && maskDiv.matches('div[data-testid="beast-core-modal-mask"]')) {
                maskDiv.parentNode.removeChild(maskDiv);
            }

        } else {
            log('找到包含 "调价原因" 或 "切换店铺" 的元素');
        }

        //结束后删除空的div
        setTimeout(function() {
            // 调用函数以删除body尾部的空div
            removeEmptyDivsAtBodyEnd();
        }, WaitTime + FastDuration + 10000); // 延迟：毫秒

    });
}

// insertModalDivs 函数的定义（如果它在其他地方没有定义的话）
function insertModalDivs(text) {
    // 这里是插入模态框的代码
    // 插入遮罩层 div
    const newDiv0 = document.createElement('div'); //创建div元素
    newDiv0.id = 'js_info'; // 设置id为js_info
    newDiv0.setAttribute('data-testid', 'beast-core-modal-mask'); //设置属性值"data-testid"的值为"beast-core-modal-mask"
    newDiv0.setAttribute('class', 'MDL_mask_5-109-0');
    newDiv0.setAttribute('style', 'z-index: 2147483647 !important; position: fixed; top: 0; left: 0; right: 0; bottom: 0; background-color: rgba(0, 0, 0, 0.5); /* 示例背景色，可以根据需要调整 */');

    // 将遮罩层 div 添加到 body 的末尾
    document.body.appendChild(newDiv0);

    // 插入模态框 div
    const newDiv = document.createElement('div');
    newDiv.id = 'js_info'; // 设置id为js_info
    newDiv.setAttribute('data-testid', 'beast-core-modal');
    newDiv.setAttribute('class', 'MDL_outerWrapper_5-109-0 MDL_alert_5-109-0 undefined');
    newDiv.setAttribute('style', 'z-index: 2147483647 !important; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%);'); // 居中显示

    // 设置模态框的内容
    newDiv.innerHTML = `
        <div data-testid="beast-core-modal-container" class="MDL_container_5-109-0">
            <div data-testid="beast-core-modal-innerWrapper" class="MDL_innerWrapper_5-109-0" style="z-index: 2147483647 !important;">
                <div data-testid="beast-core-modal-inner" class="MDL_inner_5-109-0" style="min-width: 50px; max-width: 1000px;">
                    <div data-testid="beast-core-modal-body" class="MDL_body_5-109-0 MDL_noHeader_5-109-0">
                        <div data-testid="beast-core-box">
                            <div class="modal_title"><h1>提示:</h1></div></div><br>
                        <div class="modal_body_content">
                            来自 拼多多跨境(Temu)弹窗屏蔽 js用户脚本提示：\n
                            <br>
                            ${text} <!-- 这里插入传入的 text 变量 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `;
    //网页结构复用拼多多中心消息提示

    // 将模态框 div 添加到 body 的末尾（通常在遮罩层之后）
    document.body.appendChild(newDiv);
}

// 防抖函数实现
function debounce(func, wait) {
    let timeout;
    return function() {
        const context = this;
        const args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(() => func.apply(context, args), wait);
    };
}

//监听切换功能区
// 获取.index-module__menuBox___2aaTA元素，并为其添加click事件监听器
//并且参数传递到debounce进行去重
document.querySelector('.index-module__menuBox___2aaTA').addEventListener('click', debounce(function(event) {
    if (event.target.closest('.index-module__menuBox___2aaTA')) {
        // 检查点击的目标是否是.bg-shell-theme-menu-mms的后代
        var mmsDescendant = event.target.closest('.bg-shell-theme-menu-mms');
        if (mmsDescendant) {
            // 进一步检查点击的目标是否是.index-module__menu___3Wyz- .bg-shell-theme-menu的后代
            // 但不是.bg-shell-theme-menu-mms的直接后代
            var menuDescendant = event.target.closest('.index-module__menu___3Wyz- .bg-shell-theme-menu');
            if (menuDescendant && mmsDescendant !== menuDescendant.parentNode) {
                // 这里表示匹配成功
                log('匹配到.index-module__menu___3Wyz- .bg-shell-theme-menu .bg-shell-theme-menu-mms下的.index-module__menu___3Wyz- .bg-shell-theme-menu的后代元素触发的点击');

                // 可以在这里添加点击事件的处理逻辑
                var longtimelist = [
                    '/goods/product/list',
                    '/main/sale-manage/main'
                ];
                setTimeout(function() {
                    log(`等待 100 毫秒,加载网址进行匹配`);
                    var currentPath = window.location.pathname; // 路径部分
                    let time; //初始化变量
                    if (longtimelist.includes(currentPath)) {
                        time = 1400;
                    } else {
                        time = 960;
                    }//判断是否匹配长时间列表

                    setTimeout(function() {
                        log(`已经等待 ${time} 毫秒,执行删除函数`);
                        // 调用函数以删除body尾部的空div
                        removeElements();
                        setTimeout(function() {
                            CheckWebError();
                        }, 1000); // 延迟：毫秒
                    }, time); // 延迟：毫秒
                }, 100); // 延迟：毫秒
            }
        }
    }
}, 1000));

// 定义删除尾部空div
function removeEmptyDivsAtBodyEnd() {
    // 获取body元素的最后一个子节点
    let lastChild = document.body.lastChild;

    // 循环检查body的最后一个子节点，直到找到非div或遇到空的div
    while (lastChild && lastChild.nodeName.toLowerCase() === 'div') {
        // 检查textContent是否为空字符串（忽略空格和换行符）
        if (!lastChild.textContent.trim()) {
            // 如果为空，则移除它
            lastChild.parentNode.removeChild(lastChild);
            // 更新lastChild为新的最后一个子节点
            lastChild = document.body.lastChild;
        } else {
            // 如果不是空的div，则停止循环（因为已经找到了非空或者非div的节点）
            break;
        }
    }
}

function CheckWebError() {

    const rootDiv = document.getElementById('root');
    if (!rootDiv) {
        log('未找到ID为"root"的div元素');
        return;
    }

    const commentsToFind = [
        '<!--- script https://bstatic.cdnfe.com/static/main/maihuo/static/js/bgb-sc-main/runtime~main.ce42606d.js replaced by import-html-entry --->',
        '<!--- script https://bstatic.cdnfe.com/static/main/maihuo/static/js/bgb-sc-main/48.209360ea.chunk.js replaced by import-html-entry --->',
        '<!--- script https://bstatic.cdnfe.com/static/main/maihuo/static/js/bgb-sc-main/main.a38fe5f9.chunk.js replaced by import-html-entry --->'
    ];

    let allCommentsFound = true;

    // 遍历div[id="root"]下的前三个元素（或所有子元素）
    for (let i = 0; i < Math.min(3, rootDiv.childNodes.length); i++) {
        const childNode = rootDiv.childNodes[i];

        // 检查文本节点或元素节点的文本内容是否包含commentsToFind中的注释
        if (childNode.nodeType === Node.TEXT_NODE || childNode.nodeType === Node.ELEMENT_NODE) {
            if (!commentsToFind.some(comment => childNode.textContent.includes(comment.trim()))) {
                allCommentsFound = false;
                break; // 找到一个不匹配的就可以中断循环
            }
        }

        // 如果需要直接检查注释节点（通常不常见，因为注释不是DOM元素）
        // 需要额外的逻辑来解析文本内容中的注释
        // 这里只是一个示例，实际情况可能更复杂
        else if (childNode.nodeType === Node.COMMENT_NODE) {
            // 检查注释节点的nodeValue是否等于commentsToFind中的注释
            if (!commentsToFind.includes(childNode.nodeValue)) {
                allCommentsFound = false;
                break; // 找到一个不匹配的就可以中断循环
            }
        }
    }

    if (allCommentsFound) {
        log('找到所有定义的错误注释,刷新网页');
        location.reload(); // 刷新页面
    } else {
        log('未找到所有定义的错误注释,保持原样');
    }

}

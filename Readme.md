<p align=center>
  <img src="https://gitlab.com/linying23333/green-service-center-temu-or-pinduoduokuajing-2024/-/raw/main/icon.svg"/ width="200px" height="200px">
  <br>拼多多跨境(Temu)弹窗屏蔽，一款主打 Tampermonkey 插件的 拼多多跨境(Temu)卖家 绿化JavaScript用户脚本。
</p>

# ⚠️❗❗❗作者注:
##### 作者非全职程序员，只是个会用搜索引擎写小脚本的摆烂人。可以提意见，但是不一定能实现(能力不足...)
##### 脚本写的很简单粗暴，仅仅只是自用分享。勿要要求太多
##### 有想法想以fork的方式修改完善该脚本可以通知与我
##### ⚠️未来可能因为环境原因随时弃坑
##### ⚠️⚠️⚠️免责声明：使用该脚本造成的任何损失与版权纠纷等后果与作者无关

### 🏠 兼容性

拼多多跨境(Temu)弹窗屏蔽 是一个优化 拼多多跨境卖家中心的推广广告、使用体验的脚本。  
在使用脚本之前，你需要为浏览器安装脚本管理器/加载器插件。  
经过测试，我们确定兼容以下浏览器和插件用于安装此脚本。

#### 🌐 浏览器支持（任选其一即可）

* Chrome 或 基于 Chromium 内核的浏览器 (Edge、360浏览器、CentBrowser等) <sup>*推荐</sup>  
* Firefox <sup>*功能兼容，但可能并非最佳性能</sup>

##### 📱 手机端浏览器推荐: <sup>*未经授权，如有侵犯您的相关权利请与我沟通</sup>
* <img src="https://viayoo.com/en/images/logo.png" alt="Via Logo" width="25px">Via：<a href="http://viayoo.com/zh-cn">跳转</a><sup>*经过测试</sup>

##### 优点：安装包超小，使用内建的油猴脚本解析方式 | 缺点：功能少，并且调用手机内置WebView浏览器组件可能网页导致加载不完整

##### 以下资源均为百度推荐，未经测试请自行判断

* <img src="https://www.xbext.com/icons/favicon-16x16.png" alt="X browser Logo" width="25px">X浏览器：<a href="https://www.xbext.com/">跳转</a>

#### 下方是需要安装扩展的手机浏览器推荐

* <img src="https://raw.githubusercontent.com/kiwibrowser/src.next/kiwi/kiwi_logo_circle.svg" alt="Kiwi browser Logo" width="25px">Kiwi 浏览器：<a href="https://kiwibrowsercn.github.io/">跳转</a>
* <img src="https://www.firefox.com.cn/media/protocol/img/logos/firefox/browser/logo.eb1324e44442.svg" alt="Firefox browser Logo" width="25px">Firefox 火狐浏览器：<a href="https://www.firefox.com.cn/">跳转</a>

##### 手机端需要使用设置菜单您可能需要先设置代码段
```
 }, GM_getValue('setting_Quickly_Click_3Times_To_Open_Settings_Check_Time')); // 使用存储的检查时间
```
    
##### 更改为
```
 }, 这里是你想要设置的等待时间，比如5000); // 使用存储的检查时间
```
   
#### 🙈 浏览器需要安装的脚本管理器插件（任选其一即可）(如果您的打开脚本下载链接会跳脚本安装请不要重复安装)

* Tampermonkey BETA (新油猴测试版) (又名:篡改猴测试版) <sup>*<a href="https://www.tampermonkey.net/">插件安装链接</a> </sup> <sup> * 推荐</sup>

* Tampermonkey (新油猴) (又名:篡改猴) <sup>*<a href="https://www.tampermonkey.net/">插件安装链接</a></sup>  
* Script Cat (脚本猫) (中国社区开发) <sup>*<a href="https://docs.scriptcat.org/">插件安装链接</a></sup>
* Violentmonkey (暴力猴) <sup>*<a href="https://violentmonkey.github.io/get-it/">插件安装链接</a></sup> 
* Greasemonkey (旧油猴) <sup>*<a href="https://addons.mozilla.org/firefox/addon/greasemonkey/">插件安装链接</a></sup> </br> <sup>注：Greasemonkey (旧油猴) 可能由于过于古老，无法支持该脚本。</sup> 

#### 🧪 已知支持的操作系统

* Windows、Android 等支持 Chrome 或 Firefox 浏览器的所有操作系统 <sup>*移动平台如 iOS 除外</sup>

### 📄 安装脚本	

* ~~从 Github 安装~~<sup>*账号被封</sup>

 ~~通过 Github 直接安装脚本: **[点我](https://github.com/linying2333/green-service-center-temu-or-pinduoduokuajing-2024/raw/main/temu.user.js)**~~

* 从 GitLab 安装

通过 GitLab 直接安装脚本: **[点我](https://gitlab.com/linying23333/green-service-center-temu-or-pinduoduokuajing-2024/raw/main/temu.user.js)**  

无jQuery且localStorage存储 特别版下载: **[点我](https://gitlab.com/linying23333/green-service-center-temu-or-pinduoduokuajing-2024/raw/main/%E6%8B%BC%E5%A4%9A%E5%A4%9A%E8%B7%A8%E5%A2%83(Temu)%E5%BC%B9%E7%AA%97%E5%B1%8F%E8%94%BD-localStorageAndNojQuery.user.js)**

* 从 GreasyFork 安装

通过 GreasyFork 安装脚本: **[点我](https://greasyfork.org/zh-CN/scripts/496221)**  

* 从 ScriptCat 安装

通过 ScriptCat 安装脚本: **[点我](https://scriptcat.org/zh-CN/script-show-page/1880)**

* ~~从 OpenUserJS 安装~~<sup>*暂未上传</sup>

 ~~通过 OpenUserJS 安装脚本: **[点我](https://openuserjs.org/)**~~

 ~~然后点击页面右上角的`Install`进行安装.~~

### 💬 ❓❓❓ 发现问题？
请在Github提出"issues"或者对应下载平台进行评价反馈,会尽量尝试解决

### 🚀 贡献 (Github被封暂时仅用GitLab)
##### ~~原作者Github链接:  **[点我](https://github.com/linying2333)**~~ <sup>*账号被封</sup>
##### 原作者GitLab链接:  **[点我](https://gitlab.com/linying23333)**
##### ~~原作者Github本项目库:  **[点我](https://github.com/linying2333/green-service-center-temu-or-pinduoduokuajing-2024)**~~ <sup>*账号被封</sup>
##### 原作者GitLab本项目库:  **[点我](https://gitlab.com/linying23333/green-service-center-temu-or-pinduoduokuajing-2024)**
欢迎对本项目提交“Issues”帮助我完善脚本；  
如果你对JS有所了解，可以直接在GitLab提交**Merge requests**或者Github提交**Pull Requests**，要求如下：

修改信息：

* 修改版本号  

##### 功能性更改请第二位+1，bug修复请第三位+1，第一位为作者保留
   
##### 例：0.1 => 0.1.1 ; 0.1 => 0.2
```
// @version      0.2.1
```

* 将更新描述在 ` // ==/UserScript== ` 下方最上行加上一行，修改版本号（要求同上），并修改更新描述：

```
// @note         2024-06-01 08:00:00 UTC v0.1 （您的更新描述）
```

* 将更新描述在仓库 ` Readme.md ` 的 ` 📝 更新日志： ` 下方内最上行加上一行，修改版本号（要求同上），并修改更新描述：

```
2024-06-01 08:00:00 UTC
v0.1
(您的更新描述)
```
* 注：允许进行修改者署名

### 🐛 已知问题:

##### Q1.有概率提示模块异常,白屏等
##### A1.暂时无法解决,请刷新一下网页
<br></br>
##### Q2.为什么这些数值不能填0
```
//快速删除间隔时间
let FastIntervalTime = 1; // 单位毫秒,1秒 = 1000毫秒
//快速删除持续删除时间
let FastDuration = 1; // 单位毫秒,1秒 = 1000毫秒
```
##### A2.机制限制,不打算解决
<br></br>
##### Q3.按钮位置不定?
##### A3.按钮位置是根据设置的位置百分比计算生成的，所以会自动根据浏览器窗口大小变动位置
<br></br>
##### Q4.网页按键被遮挡
##### A4.同A3，可以使用"Ctrl+鼠标滚轮"来缩放解决，后续可能考虑制作一个菜单自定义位置
<br></br>
##### Q5.说明文档乱版？
##### A5.暂时算是尽力了，每个网站加载方式不一样，解决不了没意义


### 🌇 未来打算实现的功能

##### 1.帮助拒绝降价(可选)

### 📝 更新日志：
<pre>
2024/07/05 11:28:24 UTC
v1.1 & NojQuery-0.8
更新颜色反转功能

2024/07/02 03:33:57 UTC
开了一个新分支版本（无jQuery并且使用浏览器本地存储）
NojQuery-0.7

2024/06/09 20:05:44 UTC
v1.0
用GPT以jQuery 2.2.4完全重构了代码

2024/06/09 15:31:50 UTC
v0.6
1.实现窗口设置参数
2.实现油猴存储参数，可以保留数据升级了
(可能降低浏览器兼容性，如果不能运行可回退v0.5.6.1)

2024/06/07 10:31:35 UTC
v0.5.6.1
1.为自动删除上调了0.2秒的延迟
2.更新默认按钮带单位
3.更新测试版描述,并同时发布测试版

2024/06/06 15:54:40 UTC
v0.5.6
修复了写错了的网页错误自动刷新

2024/06/06 15:54:40 UTC
v0.5.5
重新构造按钮，可以自由拖动并且始终显示在最前面(先需要点击一次方可拖动)

2024/06/06 03:18:20 UTC
v0.5.4
简单重构自动刷新(测试版未正式发布)

2024/06/05 05:32:11 UTC
v0.5.3
1.创建了GitLab项目库
2.监测模态框,当弹出新窗口时自动重新添加手动清除按钮，使其保持可点击

2024/06/03 11:25:25 UTC
v0.5.2
1.使用变量设置按钮位置，允许设置是否添加按钮
2.说明文档使用markdown

2024/05/31 08:44:20 UTC
v0.5.1
1.将海外中心面单生成网址加入脚本生效黑名单
2.将控制台日志改为启用方可显示

2024/05/30 08:46:26 UTC
v0.5
1.添加了当网页白屏时自动刷新
2.将海外服务中心的账号授权网页加入脚本生效黑名单

2024/05/28 02:28:06 UTC
脚本提交到脚本猫同名作者
https://scriptcat.org/zh-CN/script-show-page/1880

2024/05/28 02:02:10 UTC
v0.4.2
更改了删除顺序,防止模块异常

2024/05/28 00:19:42 UTC
v0.4.1
手动清除按钮跳过等待一段时间清除

2024/05/27 10:58:39 UTC
v0.4
添加了一个手动清除按钮

2024/05/27 10:58:39 UTC
脚本提交到greasyfork同名作者
https://greasyfork.org/zh-CN/scripts/496221

2024/05/26 03:54:17 UTC
v0.3.2
修复了因为点击过快触发的bug

2024/05/25 17:04:18 UTC
v0.3.1
修复上版本重新删除弹窗，当切换功能被点击时再次执行删除

2024/05/25 12:12:32 UTC
v0.3
1.将店铺切换也加入不删除白名单
2.监听网页,当处于可能弹窗的相关网页被点击时再次执行删除
3.添加了清理"boby"尾部空的元素
4.网站套上了cloudflare

2024/05/24 09:00:00 UTC
v0.2
添加了持续一段时间循环删除,并正式发布使用

2024/05/23 17:49:12 UTC
v0.0
完全构建了操作方式,实现了可控的仅保留旧版价格调价面板显示,在网页上显示固定提示

2024/05/22 06:00:00 UTC
v0
首个js用户脚本诞生，但由于操作方式过于粗暴导致仅能用于查看
</pre>
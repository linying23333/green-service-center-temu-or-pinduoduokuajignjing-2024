// ==UserScript==
// @name         [改]砍Temu999刀(一键拒绝降价)
// @namespace    http://greasyfork.org/
// @version      3.1.1
// @description  Temu网页端一键拒绝所有降价，是兄弟就来砍我
// @author       menkeng & linying
// @license      GPLv3
// @match        *://kuajing.pinduoduo.com/*
// @match        *://seller.kuajingmaihuo.com/*
// @match        *://agentseller.temu.com/*
// @exclude      https://seller.kuajingmaihuo.com/login*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @downloadURL  https://update.greasyfork.org/scripts/483394/%E7%A0%8DTemu999%E5%88%80%EF%BC%88%E4%B8%80%E9%94%AE%E6%8B%92%E7%BB%9D%E9%99%8D%E4%BB%B7%EF%BC%89.user.js
// @updateURL    https://update.greasyfork.org/scripts/483394/%E7%A0%8DTemu999%E5%88%80%EF%BC%88%E4%B8%80%E9%94%AE%E6%8B%92%E7%BB%9D%E9%99%8D%E4%BB%B7%EF%BC%89.meta.js
// @run-at       document-idle
// @run-at       //context-menu
// @grant        none
// ==/UserScript==
//运行方式：主动监测降价提醒，点击按钮一键拒绝
//小红标太烦了，顺手砍了
//menkeng:
//脚本定制Q:605011383
//脚本定制Q:605011383
//脚本定制Q:605011383

(function() {
    'use strict';

// 设置一个定时器
setTimeout(function() {
    // 弹窗,需要点了才会接着运行
    alert(`已经加载一键拒绝降价`);

        var check_price_interval;
        var check_price_flag = false
        window.onload = function () {
            check_price_cut()
        }
        function check_price_cut() {
            var check_price_time = 0
            check_price_interval = setInterval(() => {
                check_price_time++
                var flexs = document.querySelectorAll("div.MDL_header_5-109-0")
                flexs.forEach(function (pop) {
                    if (pop.innerText == "商品降价提醒") {
                        pop = pop.nextElementSibling
                        console.log("捕捉到降价提醒")
                        if (check_price_time > 10) {
                            clearInterval(check_price_interval)
                        }
                        check_price_flag = true
                        clearInterval(check_price_interval)
                        var header = pop.querySelector('[class^="price-adjust-confirm_header"]')
                        var item_counts = header.innerText.match(/\d+/)
                        console.log("共有" + item_counts + "个商品")
                        var reject_btn = document.createElement("div");
                        reject_btn.innerText = "拒绝降价";
                        reject_btn.id = "reject_btn";
                        reject_btn.style.position = "fixed";
                        reject_btn.style.top = "20%";
                        reject_btn.style.right = "22%";
                        reject_btn.style.zIndex = "9999";
                        reject_btn.style.display = "flex";
                        reject_btn.style.borderRadius = "5px";
                        reject_btn.style.backgroundColor = 'rgba(251, 119, 1,1)';
                        reject_btn.style.color = 'white';
                        reject_btn.style.padding = '10px 15px';
                        reject_btn.style.border = 'none';
                        reject_btn.style.cursor = 'pointer';
                        reject_btn.addEventListener("click", function () {
                            price_cut();
                        });
                        document.body.appendChild(reject_btn)
                        function price_cut() {
                            var item_list = pop.querySelectorAll("tbody > tr.TB_tr_5-109-0")
                            var item_count = item_list.length
                            var reject_count = 0
                            var time = 0
                            while (reject_count < item_counts && time < 30) {
                                var scrollableDiv = pop.querySelector(".TB_body_5-109-0 > div")
                                scrollableDiv.scrollTop += 300
                                var scrollEvent = new CustomEvent('scroll')
                                scrollableDiv.dispatchEvent(scrollEvent)
                                time++
                                item_list = pop.querySelectorAll("tbody > tr.TB_tr_5-109-0")
                                item_count = item_list.length

                                let reject_count
                                let time
                                item_list.forEach(function (item) {
                                    console.log(time + " \t" + item.innerText.match(/\d+/g)[0])
                                    var reject = item.querySelector("td:last-child > div > label:last-child > div:last-child")
                                    var reject_state = reject.previousElementSibling.querySelector("div").classList
                                    if (!reject_state.contains('RD_active_5-109-0')) {
                                        reject_count++
                                        reject.click()
                                        reject.style.color = "#ff0000"
                                        console.log(reject_count + "个商品已拒绝")
                                    }
                                    var pp = document.querySelector(".PT_portalMain_5-109-0.PP_popoverMain_5-109-0")
                                    if (pp) {
                                        var check = pp.querySelector(".CBX_square_5-109-0.CBX_groupDisabled_5-109-0.CBX_hasCheckSquare_5-109-0.CBX_mount_5-109-0")
                                        var accept = pp.querySelector(".BTN_outerWrapper_5-109-0.BTN_danger_5-109-0.BTN_medium_5-109-0.BTN_outerWrapperBtn_5-109-0")
                                        check.click()
                                        accept.click()
                                        document.querySelector("#reject_btn").remove()
                                    }
                                })
                            }

                        }

                    }
                })
            }, 1000)

        }
        setTimeout(() => {
            var kill = document.querySelectorAll('span[data-testid="beast-core-badge-count"]')
            kill.forEach(element => {
                element.remove()
            })
        }, 2000);

}, 1000); // 延迟  毫秒

    // Your code here...
})();
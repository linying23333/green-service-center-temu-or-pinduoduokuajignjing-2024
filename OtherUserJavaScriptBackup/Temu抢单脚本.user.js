// ==UserScript==
// @name         Ship
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Temu批量自动点击加入发货台
// @author       使用需自行承担风险,如安装好不生效请重启浏览器后多刷新几次页面
// @match        https://kuajing.pinduoduo.com/*
// @icon         https://kj-bstatic.pddpic.com/static/sc-container/favicon.ico
// @grant        none
// ==/UserScript==

let _MyInterval = null
let newButton = document.createElement('button')

function createButton(){
  let target = document.querySelector('.bg-shell-theme-userInfo')
  newButton.innerHTML = '一键抢发货台'
  newButton.className = 'BTN_outerWrapper_5-52-0 BTN_primary_5-52-0 BTN_medium_5-52-0 BTN_outerWrapperBtn_5-52-0'
  newButton.onclick = function() {
    if (_MyInterval) {
      clearInterval(_MyInterval)
      _MyInterval = null
      newButton.innerHTML = '一键抢发货台'
    } else {
      let value = document.querySelector('#_MyShip').value
      _MyInterval = setInterval(run, value * 1000)
      newButton.innerHTML = '停止'
    }
  }
  let newInput = document.createElement('input')
  newInput.setAttribute('id', '_MyShip')
  newInput.setAttribute('type', 'number')
  newInput.setAttribute('value', 3)
  newInput.setAttribute('min', 1)
  newInput.style.width = '30px'
  let newDiv = document.createElement('div')
  newDiv.style = 'margin-right: 16px'
  newDiv.appendChild(newButton)
  newDiv.appendChild(newInput)
  if (target) {
    target.insertBefore(newDiv, target.firstChild)
  }
}

function run(){
    let fahuotao = []
    let queren = []
    let aList = document.querySelector('tbody[data-testid="beast-core-table-middle-tbody"]').querySelectorAll('a')
    let ready = false

    function sleep(time){
        return new Promise((resolve) => setTimeout(resolve, time))
    }

    for(let i=0; i<aList.length; i++){
        if (aList[i].innerText === '加入发货台' && !([...aList[i].classList].includes('BTN_disabled_5-52-0'))) {
            fahuotao.push(aList[i])
        }
    }

    for(let i=0; i<fahuotao.length; i++) {
        (async function(){
            fahuotao[i].click()
            await sleep(1000)
            queren = document.querySelectorAll('div[data-testid="beast-core-portal"]')
            if (i == fahuotao.length - 1){
                for(let j=0; j<queren.length; j++) {
                    queren[j].querySelector('button').click()
                }
            }
        })()

    }

    if (fahuotao.length === 0) {
      alert('没找到可用的加入发货台订单')
        clearInterval(_MyInterval)
        _MyInterval = null
        newButton.innerHTML = '一键抢发货台'
    }

}

(function() {
    'use strict';
    (function(){
      window.onload = function(){
        createButton()
      }
      
    })()
    // Your code here...
})();